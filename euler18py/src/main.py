# -*- coding: utf-8 -*-
'''
 Author      : Anders Fl�e Andersen
 Version     :
 Copyright   :
 Description : Euler 18

 By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

 3
 7 4
 2 4 6
 8 5 9 3

 That is, 3 + 7 + 4 + 9 = 23.

 Find the maximum total from top to bottom of the triangle below:

 75
 95 64
 17 47 82
 18 35 87 10
 20 04 82 47 65
 19 01 23 75 03 34
 88 02 77 73 07 63 67
 99 65 04 28 06 16 70 92
 41 41 26 56 83 40 80 70 33
 41 48 72 33 47 32 37 16 94 29
 53 71 44 65 25 43 91 52 97 51 14
 70 11 33 28 77 73 17 78 39 68 17 57
 91 71 52 38 17 14 91 43 58 50 27 29 48
 63 66 04 68 89 53 67 30 73 16 69 87 40 31
 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

 NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)

'''

class node:
    def __init__(self, value, row):
        self.above_left = None
        self.above_right = None
        self.below_left = None
        self.below_right = None
        self.value = value  
        self.row = row

def parseData(data):
    data = data.splitlines()
    numRows = len(data)
    for i in xrange(numRows):
        data[i] = data[i].strip()
        data[i] = data[i].split(" ")
    for i in xrange(len(data) - 1):
        for j in xrange(len(data[i])):
            if i == 0:
                data[i][j] = node(int(data[i][j]), i)
            if not(isinstance(data[i+1][j], node)):
                n = node(int(data[i+1][j]), i+1)
            else:
                n = data[i+1][j]
            data[i][j].below_left = n
            data[i+1][j] = data[i][j].below_left
            data[i][j].below_left.above_right = data[i][j]
            if not(isinstance(data[i+1][j+1], node)):
                nn = node(int(data[i+1][j+1]), i+1)
            else:
                nn = data[i+1][j+1]
            data[i][j].below_right = nn
            data[i+1][j+1] = data[i][j].below_right
            data[i][j].below_right.above_left = data[i][j]
    return data[0][0], numRows

def bestPath(data, numRows):
    cache = [0 for _ in xrange(numRows)]
    parseTree(0, data, cache)
    return cache[numRows-1]
    
def parseTree(parentsum, node, cache):
    sum = parentsum + node.value
    if(sum > cache[node.row]):
        cache[node.row] = sum
    if not(node.below_left == None):
        parseTree(sum, node.below_left, cache)
    if not(node.below_right == None):
        parseTree(sum, node.below_right, cache)    
    
data = """75
     95 64
     17 47 82
     18 35 87 10
     20 04 82 47 65
     19 01 23 75 03 34
     88 02 77 73 07 63 67
     99 65 04 28 06 16 70 92
     41 41 26 56 83 40 80 70 33
     41 48 72 33 47 32 37 16 94 29
     53 71 44 65 25 43 91 52 97 51 14
     70 11 33 28 77 73 17 78 39 68 17 57
     91 71 52 38 17 14 91 43 58 50 27 29 48
     63 66 04 68 89 53 67 30 73 16 69 87 40 31
     04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"""
if __name__ == "__main__":
    tree, numRows = parseData(data)
    max = bestPath(tree, numRows)
    print max
    

    
    