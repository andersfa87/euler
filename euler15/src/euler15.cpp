//============================================================================
// Name        : euler15.cpp
// Author      : Anders Fløe Andersen
// Version     :
// Copyright   :
// Description : Euler 15
//============================================================================

#include <iostream>
using namespace std;

const int width = 20;
const int height = 20;
long cache [width + 1][height + 1];

long get_paths(int x, int y){
	if(cache[x][y] != -1){
		return cache[x][y];
	}
	if(x == width && y == height){
		return 1;
	}
	else{
		long pathsfound = 0;
		if(x == width){
			pathsfound += get_paths(x, y+1);
		}
		else if (y == height){
			pathsfound += get_paths(x+1, y);
		}
		else{
			pathsfound += get_paths(x+1, y);
			pathsfound += get_paths(x, y+1);
		}
		cache[x][y] = pathsfound;
		return pathsfound;
	}
}

int main() {
	for(int x = 0; x <= width; x++){
		for(int y = 0; y <= height; y++){
			cache[x][y] = -1;
		}
	}
	long paths = get_paths(0, 0);
	cout << "number of paths found " << paths << endl;
	return 0;
}
