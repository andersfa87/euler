# -*- coding: utf-8 -*-
'''
 Author      : Anders Fl�e Andersen
 Version     :
 Copyright   :
 Description : Euler 67

 By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.

NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)
'''


class node:
    def __init__(self, value):
        self.above_left = None
        self.above_right = None
        self.below_left = None
        self.below_right = None
        self.value = value  
        self.maxFromThisPoint = -1

def parseData(data):
    data = data.splitlines()
    numRows = len(data)
    for i in xrange(numRows):
        data[i] = data[i].strip()
        data[i] = data[i].split(" ")
    for i in xrange(len(data) - 1):
        for j in xrange(len(data[i])):
            if i == 0:
                data[i][j] = node(int(data[i][j]))
            if not(isinstance(data[i+1][j], node)):
                n = node(int(data[i+1][j]))
            else:
                n = data[i+1][j]
            data[i][j].below_left = n
            data[i+1][j] = data[i][j].below_left
            data[i][j].below_left.above_right = data[i][j]
            if not(isinstance(data[i+1][j+1], node)):
                nn = node(int(data[i+1][j+1]))
            else:
                nn = data[i+1][j+1]
            data[i][j].below_right = nn
            data[i+1][j+1] = data[i][j].below_right
            data[i][j].below_right.above_left = data[i][j]
    return data[0][0]

def parse(data):
    return bestValueFromHere(node)    
    
def bestValueFromHere(node):
    if(node.maxFromThisPoint != -1):
        return node.maxFromThisPoint
    left = 0
    right = 0
    if not(node.below_left == None):
        left = bestValueFromHere(node.below_left)
    if not(node.below_right == None):
        right = bestValueFromHere(node.below_right)
    node.maxFromThisPoint = max(left, right) + node.value
    return node.maxFromThisPoint
    
def readFile(path):
    f = open(path, "r")
    contents = f.read()
    f.close()
    return contents

if __name__ == "__main__":
    data = readFile("triangle.txt")
    node = parseData(data)
    max = parse(node)
    print node.maxFromThisPoint