#include <iostream>
#include <math.h>
using namespace std;


/*
	Checks an integer to see if it is palidromic
*/
bool isPalindromic(int n)
{
	int temp = n;
	int remainder;
	int sum = 0;
	while(n > 0)
	{
		remainder = n % 10; //gets last digit
		n /= 10; //truncates last digit
		sum = sum*10 + remainder; //Builds value of reversed number;
	}
	if(sum == temp)
		return true;
	else
		return false;
}

/*
	A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

	Find the largest palindrome made from the product of two 3-digit numbers.
*/
int main()
{
	int result = 0;
	for(int i = 100; i <= 999; i++)
	{
		for(int j = 100; j <= 999; j++)
		{
			int product = i * j;
			if(isPalindromic(product))
			{
				if(product > result)
				{
					cout << "Found a larger palindrome from : " << i << " * " << j << " -> " << product << endl;
					result = product;
				}
			}
		}
	}
	cout << "Largest palindrome found: " << result;
	return 0;
}
