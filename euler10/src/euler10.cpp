#include <iostream>
#include <math.h>
using namespace std;

/*
	Checks wether or not n is a prime
*/
bool prime(int n)
{
	if(n < 2) return false;
	if(n > 2 && n % 2 == 0) return false;
	for (int i = 3; i <= sqrt(n); i+=2) {
		if (n % i == 0)
		    return false;
	}
	return true;
}

/*
	The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

	Find the sum of all the primes below two million.
*/
int main()
{
	long sum = 0;
	int max = 2000000;
	//int max = 10;
	for(int i = 0; i < max; i++)
	{
		if(prime(i))
			sum+=i;
	}
	cout << "Sum of primes: " << sum;
	return 0;
}
