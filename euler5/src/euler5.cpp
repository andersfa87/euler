#include <iostream>
#include <math.h>
using namespace std;

/*
	2520 is the smallest number that can be divided 
	by each of the numbers from 1 to 10 without any remainder.

	What is the smallest positive number that is 
	evenly divisible by all of the numbers from 1 to 20?
*/
int main()
{
	int max = 20;
	bool matches[max];
	bool matchfound = false;
	int counter = 1;
	while(!matchfound)
	{
		for (int i = 1; i <= max; i++)
		{
			if(counter % i == 0)
				matches[i-1] = true;			
		}
		bool anyNegatives = false;
		for (int j = 0; j < max; j++)
		{
			if(!matches[j])
				anyNegatives = true;
			matches[j] = false;
		}
		if(!anyNegatives)
		{
			matchfound = true;
		}
		else
			counter++;
	}
	cout << "Result: " << counter;
	return 0;
}
