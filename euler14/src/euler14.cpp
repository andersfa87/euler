//============================================================================
// Name        : euler14.cpp
// Author      : Anders Fløe Andersen
// Version     :
// Copyright   : 
// Description :
//============================================================================

#include <iostream>
using namespace std;

int getChainLength(long number)
{
	if(number == 1)
	{
		return 1;
	}
	else
	{
		if(number % 2 == 0)
		{
			number = number / 2;
		}
		else
		{
			number = (number * 3) + 1;
		}
		return getChainLength(number) + 1;
	}
//	int count = 0;
//	while(number != 1)
//	{
//		if(number % 2 == 0)
//			number /= 2;
//		else
//			number = (number * 3) + 1;
//		count++;
//	}
//	return count;
}

int main() {

	int million = 1000000;
	int result = 0;
	for(int i = 1; i < million; i++)
	{
		int terms = getChainLength(i);
		if(terms > result)
		{
			result = terms;
			cout << "Found higher number of terms: " << terms << " at i: " << i << endl;
		}
	}
	cout << "Longest chain: " << result << endl;
	return 0;
}


