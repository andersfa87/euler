//============================================================================
// Name        : euler13.cpp
// Author      : Anders Fløe Andersen
// Version     :
// Copyright   : 
// Description : Euler13 Problem, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <fstream>
#include <sstream>
#include <gmpxx.h>
using namespace std;

/*
	Work out the first ten digits of the sum 
	of the following one-hundred 50-digit numbers.
*/
int main()
{
	mpz_class sum = 0;
	mpz_class numbers[100];
	ifstream file ("src/euler13data.txt");
	if(file.is_open())
	{	
		int i = 0;
		string line;
		while(file.good())
		{
			getline(file, line);
			stringstream s(line);
			mpz_class val;
			s >> numbers[i];
			
			i++;
		}
	}
	else
		cout << "Unable to open file." << endl;
	for(int i = 0; i < 100; i++)
	{
		sum += numbers[i];
	}
	cout << "Result: " << sum << endl;
	stringstream s;
	s << sum;
	cout << "First 10 digits: " << s.str().substr(0,10) << endl;
	return 0;
}
