//============================================================================
// Name        : euler16.cpp
// Author      : Anders Fløe Andersen
// Version     :
// Copyright   : 
/*
	pow(2,15) = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

	What is the sum of the digits of the number pow(2,1000)?

*/
//============================================================================

#include <iostream>
#include <math.h>
#include <sstream>
#include <gmpxx.h>
using namespace std;

int main()
{
	mpz_class val = pow(2,1000);
	stringstream s;
	s << val;
	string str = s.str();
	long size = str.size();
	long sum = 0;
	for(int i = 0; i < size; i++)
	{
		stringstream stream(str.substr(i,1));
		long k;
		stream >> k;
		sum += k;
	}
	cout << "Number -> " << str << endl;
	cout << "Result of sum of digits -> " << sum << endl;
}
