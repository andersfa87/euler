# -*- coding: utf-8 -*-

'''
 Author      : Anders Fl�e Andersen
 Version     :
 Copyright   :
 Description : Euler 20

n! means n  (n  1)  ...  3  2  1

For example, 10! = 10 x 9  ...  3 x 2 x 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!

'''

if __name__ == "__main__":
    number = 100
    product = number
    s = 0
    for i in xrange(number, 0, -1):
        product *= i
    for c in product.__str__():
        s += int(c)
    
    print s