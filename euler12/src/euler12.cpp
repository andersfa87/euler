//============================================================================
// Name        : euler12.cpp
// Author      : Anders Fløe Andersen
// Version     :
// Copyright   : 
// Description :
/*
	The sequence of triangle numbers is generated 
	by adding the natural numbers. So the 7th triangle 
	number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. 
	The first ten terms would be:

	1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

	Let us list the factors of the first seven triangle numbers:

     1: 1
     3: 1,3
     6: 1,2,3,6
    10: 1,2,5,10
    15: 1,3,5,15
    21: 1,3,7,21
    28: 1,2,4,7,14,28

	We can see that 28 is the first triangle number to 
	have over five divisors.

	What is the value of the first triangle number to 
	have over five hundred divisors?
*/
//============================================================================

#include <iostream>
#include <math.h>
using namespace std;

int get_num_divisors(long n)
{
	long count = 0;
	for(long i = sqrt(n); i >= 1; i--)
		if(n % i == 0) count++;
	count*=2;
	return count;
}

int main()
{
	long i = 0;
	long last_triangle_num = 0;
	long divisor_count = 0;
	while(divisor_count <= 500)
	{
		i++;
		last_triangle_num += i;
		//cout << "Checking tnum: " << last_triangle_num << endl;
		long new_divisor_count = get_num_divisors(last_triangle_num);
		if(new_divisor_count > divisor_count)
		{
			cout << "Found larger divisor count at tnum = " << last_triangle_num << " -> " << new_divisor_count << endl;
			divisor_count = new_divisor_count;
		}
	}
	cout << "i -> " << i << endl;
	cout << "number ->" << last_triangle_num << endl;
	cout << "number of devisors -> " << divisor_count << endl;
	return 0;
}
