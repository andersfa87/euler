#include <iostream>
#include <math.h>
using namespace std;

/*
	Checks wether or not n is a prime
*/
bool prime(int n)
{
	if(n < 2) return false;
	if(n > 2 && n % 2 == 0) return false;
	for (int i = 3; i <= sqrt(n); i+=2) {
		if (n % i == 0)
		    return false;
	}
	return true;
}


/*
	The prime factors of 13195 are 5, 7, 13 and 29.

	What is the largest prime factor of the number 600851475143 ?
*/
int main(){
	
	long composite = 600851475143;
	long result;
	for(long i = 1; i < composite; i++)
	{
		if(composite % i == 0)
		{
			cout << "Found multiple " << i << ";";
			bool isPrime = prime(i);
			if(isPrime)
			{
				cout << " Is prime" << endl;
				result = i;
			}
			else
			{
				cout << " Is not a prime" << endl;
			}
		}	
	}
	cout << "Largest prime factor found was " << result << endl;
	return 0;
}
