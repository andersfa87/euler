#include <iostream>
#include <math.h>
using namespace std;

/*
	Checks wether or not n is a prime
*/
bool prime(int n)
{
	if(n < 2) return false;
	if(n > 2 && n % 2 == 0) return false;
	for (int i = 3; i <= sqrt(n); i+=2) {
		if (n % i == 0)
		    return false;
	}
	return true;
}

/*
	By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

	What is the 10001st prime number?
*/
int main()
{
	int count = 0;
	int i = 0;
	int latest;
	while(count < 10001)
	{
		if(prime(i))
		{
			latest = i;
			count++;
			cout << count << ": -> " << latest << endl;
		}
		i++;
	}
	return 0;
}
