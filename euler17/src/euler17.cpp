/*
 ============================================================================
 Name        : euler17.cpp
 Author      : Anders Fløe Andersen
 Version     :
 Copyright   :
 Description : Euler17



If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
The use of "and" when writing out numbers is in compliance with British usage.

 ============================================================================
 */

#include <iostream>

using namespace std;



string ONES[] = {"","ONE","TWO","THREE","FOUR","FIVE","SIX","SEVEN","EIGHT","NINE"};
string TENS[] = {"","TEN", "TWENTY","THIRTY","FORTY","FIFTY","SIXTY","SEVENTY","EIGHTY","NINETY"};
string TEEN[] = {TENS[1],"ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"};
string HUNDRED = "HUNDRED";
string THOUSAND = "THOUSAND";
string AND = "AND";

int getNumberCharCount(int number){
	if(number < 10){
		return ONES[number].length();
	}
	else if(number > 9 && number < 20){
		return TEEN[number-10].length();
	}
	else if(number > 19 && number < 100){
		int tens = number / 10;
		int ones = number % 10;
		return TENS[tens].length() + ONES[ones].length();
	}
	else if(number > 99 && number < 1000){
		int hundreds = number / 100;
		int tens = (number % 100) / 10;
		int ones = (number % 100) % 10;
		int length = ONES[hundreds].length() + HUNDRED.length();
		if(tens > 0 || ones > 0){
			length += AND.length();
			//10-20 faark
			if(tens == 1){
				length += TEEN[ones].length();
			}
			else{
				length += TENS[tens].length() + ONES[ones].length();
			}
		}
		return length;
	}
	else if(number == 1000){
		return ONES[1].length() + THOUSAND.length();
	}
	else{
		return 0;
	}
}

int main(void) {
	int start = 1, end = 1000;
	int result = 0;
	for(int i = start; i <= end; i++){
		result += getNumberCharCount(i);
	}
	cout << result << endl;
	return 0;
}
