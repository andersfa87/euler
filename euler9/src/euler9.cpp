#include <iostream>
#include <math.h>
using namespace std;

/*
	A Pythagorean triplet is a set of three 
	natural numbers, a < b < c, for which,
	a2 + b2 = c2

	For example, 32 + 42 = 9 + 16 = 25 = 52.

	There exists exactly one Pythagorean triplet 
	for which a + b + c = 1000.
	Find the product abc.

*/
int main()
{
	for(int a = 1; a < 1000; a++)
	{
		for(int b = a+1; b < 1000; b++)
		{
			for(int c = b+1; c < 1000; c++)
			{
				if(pow(a,2) + pow(b,2) == pow(c,2))
				{
					if(a+b+c == 1000)
					{
						cout << "Found match: " << a << "*" << b << "*" << c << "=" << a*b*c << endl;
						return 0;
					}
				}
			}
		}
	}
	return 0;
}

